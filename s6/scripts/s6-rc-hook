#!/bin/sh -e

s6_live() {
  if [ ! -L /run/s6-rc ]; then
    echo >&2 "  Skipped: s6-rc is not running."
    exit 0
  fi
}

svc_help(){
    echo "	==> Start/stop a service:"
    echo "  s6-rc -u/-d change <service>"
    echo "	==> Recompile the service database:"
    echo "  s6-db-reload <args>"
}

svc_add_help(){
    echo "	==> Add a service:"
    echo "  s6-service add default <service>"
    svc_help
}

svc_del_help(){
    echo "	==> Remove a service:"
    echo "  s6-service delete default <service>"
    svc_help
}

reload_dbus() {
    dbus-send --print-reply --system --type=method_call \
            --dest=org.freedesktop.DBus \
            / org.freedesktop.DBus.ReloadConfig > /dev/null
}

restart_service() {
  if [ -e /run/s6-rc/servicedirs/"$1" ]; then
    s6-svc -r /run/s6-rc/servicedirs/"$1"
  fi
  if [ -e /run/s6-rc/servicedirs/"$1"-srv ]; then
    s6-svc -r /run/s6-rc/servicedirs/"$1"-srv
  fi
}

each_conf() {
  while read -r f; do
    "$@" "/$f"
  done
}

op="$1"; shift

case $op in
  sysctl)   s6_live; each_conf /usr/bin/sysctl -q -p ;;
  dbus_reload) s6_live; reload_dbus ;;
    # For use by other packages
  add)      svc_add_help ;;
  del)      svc_del_help ;;
  longrun_restart) s6_live; restart_service "$1" ;;
  *) echo >&2 "  Invalid operation '$op'"; exit 1 ;;
esac

exit 0
